# pythonMETAR
Get temperature, barometric pressure, and wind speed/direction from METAR data
```
usage: weather.py [-h] [--metric] station_ids [station_ids ...]

Retrieve weather information from a METAR station and print to stdout

positional arguments:
  station_ids  Station IDs to retrieve METAR data from

optional arguments:
  -h, --help   show this help message and exit
  --metric     use metric units (default: False)

e.g. weather.py KBJC KEIK
```
Results in:
```
KBJC | 71.6 F | 29.84 inHg | VRB 4 mph
KEIK | 71.6 F | 29.79 inHg | N 0 mph
```
