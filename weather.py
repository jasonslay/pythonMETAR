import argparse
import requests
import re


class METAR:
    def __init__(self, station_id, use_metric_units=False):
        self.station_id = station_id
        self.use_metric_units = use_metric_units
        self.update()

    @property
    def temperature(self):
        return self.temp_c if self.use_metric_units else self.temp_c * 9.0 / 5.0 + 32.0

    @property
    def temperature_unit(self):
        return 'C' if self.use_metric_units else 'F'

    @property
    def wind_direction(self):
        if self.wind_az == 'VRB':
            return 'VRB'
        else:
            self.wind_az = float(self.wind_az) % 360.0
            if self.wind_az <= 11.25 or self.wind_az >= 348.75:
                return 'N'
            elif self.wind_az < 33.75:
                return 'NNE'
            elif self.wind_az <= 56.25:
                return 'NE'
            elif self.wind_az < 78.75:
                return 'ENE'
            elif self.wind_az <= 101.25:
                return 'E'
            elif self.wind_az < 123.75:
                return 'ESE'
            elif self.wind_az <= 146.25:
                return 'SE'
            elif self.wind_az < 168.75:
                return 'SSE'
            elif self.wind_az <= 191.25:
                return 'S'
            elif self.wind_az < 213.75:
                return 'SSW'
            elif self.wind_az <= 236.25:
                return 'SW'
            elif self.wind_az < 258.75:
                return 'WSW'
            elif self.wind_az <= 281.25:
                return 'W'
            elif self.wind_az < 303.75:
                return 'WNW'
            elif self.wind_az <= 326.25:
                return 'NW'
            else:
                return 'NNW'

    @property
    def wind_speed(self):
        return int(self.wind_speed_kts * 1.852) if self.use_metric_units else int(self.wind_speed_kts * 1.15078)

    @property
    def wind_gust_speed(self):
        if self.wind_gust_kts is not None:
            return int(self.wind_gust_kts * 1.852) if self.use_metric_units else int(self.wind_gust_kts * 1.15078)

    @property
    def barometric_pressure(self):
        return self.baro_inhg * 25.4 if self.use_metric_units else self.baro_inhg

    @property
    def barometric_pressure_unit(self):
        return 'mmHg' if self.use_metric_units else 'inHg'

    @property
    def speed_unit(self):
        return 'kph' if self.use_metric_units else 'mph'

    def __str__(self):
        if self.error:
            return '{ID} | {error}'.format(
                ID=self.station_id,
                error=self.error)
        return '{ID} | {temp} {temp_unit} | {baro} {baro_unit} | {direction} {speed}{gusts} {speed_unit}'.format(
            ID=self.station_id,
            temp='{0:.1f}'.format(self.temperature),
            temp_unit=self.temperature_unit,
            baro='{0:.2f}'.format(self.barometric_pressure),
            baro_unit=self.barometric_pressure_unit,
            direction=self.wind_direction,
            speed=self.wind_speed,
            gusts='G' + str(self.wind_gust_speed) if self.wind_gust_speed is not None else '',
            speed_unit=self.speed_unit)

    def update(self):
        self.error = None
        req = requests.get('http://w1.weather.gov/data/METAR/' + self.station_id + '.1.txt')
        if req.status_code != 200:
            self.error = 'Unable to retreive METAR data!'
        raw = req.text.replace('\n', ' ')
        try:
            temp_c = re.findall('M?\d+\/M?\d+ A',  raw)[0].rstrip('A').split('/')[0]
            if temp_c[0] == 'M':
                temp_c = float(temp_c.lstrip('M')) * -1
            else:
                temp_c = float(temp_c)
            raw_baro = re.findall('A[0-9]{4}', raw)[0].strip('A')
            baro = float(raw_baro[:2] + '.' + raw_baro[2:])
            raw_wind = re.findall('(?:VRB|[0-9]{3})[0-9]{2}(?:G[0-9]+)?KT', raw)[0].strip('KT')
            wind_az = raw_wind[:3]
            wind_speed_kts = int(raw_wind.split('G')[0][3:])
            if 'G' in raw_wind:
                wind_gust_kts = int(raw_wind.split('G')[1])
            else:
                wind_gust_kts = None
        except IndexError:
            self.error = 'Unable to parse METAR data!'
        else:
            self.temp_c = temp_c
            self.baro_inhg = baro
            self.wind_az = wind_az
            self.wind_speed_kts = wind_speed_kts
            self.wind_gust_kts = wind_gust_kts


if __name__ == '__main__':
    # Argument parser
    parser = argparse.ArgumentParser(
        description='Retrieve weather information from a METAR station and print to stdout',
        epilog='e.g. weather.py KBJC KEIK',
        formatter_class=argparse.ArgumentDefaultsHelpFormatter)

    parser.add_argument('station_ids',
                        nargs='+',
                        type=str,
                        help='Station IDs to retrieve METAR data from')

    parser.add_argument('--metric',
                        action='store_true',
                        help='use metric units')

    args = vars(parser.parse_args())
    for station_id in args['station_ids']:
        print(METAR(station_id, use_metric_units=args['metric']))
